<?php

require_once(_ROOT_PATH_.'core/Model.php');

/**
 * Configuration table model
 */
class Configuration extends Model
{
	protected $nom;
	protected $valeur;

	public function __construct($id = null)
	{
		$this->definition = array(
			'table'   => array('Configuration'),
			'primary' => 'idConfig',
			'fields'  => array('nom', 'valeur')
		);

		parent::__construct($id);
	}

	/**
	 * Returns the value of the specified configuration field
	 * @param string $name Name of the configuration field
	 * @return string Configuration value
	 */
	public function get($name)
	{
		$this->db->connect();

		$query = "SELECT valeur 
			FROM Configuration
			WHERE nom = '".$name."'";

		$result = $this->db->getRow($query);
		$this->db->close();

		return $result['VALEUR'];
	}
}