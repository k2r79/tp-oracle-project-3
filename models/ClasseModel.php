<?php

require_once(_ROOT_PATH_.'core/Model.php');
require_once(_ROOT_PATH_.'models/EleveModel.php');

/**
 * Configuration table model
 */
class Classe extends Model
{
	protected $libelle;
	protected $eleves = array();

	public function __construct($id = null)
	{
		$this->definition = array(
			'table'   => array('Classe'),
			'primary' => 'idClasse',
			'fields'  => array('libelle')
		);
		parent::__construct($id);

		if ($this->id) {
			$this->fetchEleves();
		}
	}

	/**
	 * Collects all the students in the class from the database
	 */
	public function fetchEleves()
	{
		$this->db->connect();

		$query = "SELECT idEleve 
			FROM Eleve
			WHERE idClasse = ".$this->id;

		$results = $this->db->query($query);

		while ($row = oci_fetch_assoc($results)) {
			$this->eleves[] = new Eleve($row['IDELEVE']);
		}

		$this->db->close();
	}

	public function getLibelle()
	{
		return $this->libelle;
	}

	public function getEleves()
	{
		return $this->eleves;
	}
}