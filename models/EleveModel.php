<?php

require_once(_ROOT_PATH_.'core/Model.php');
require_once(_ROOT_PATH_.'models/UtilisateurModel.php');

/**
 * Eleve table model
 */
class Eleve extends Utilisateur
{
	protected $idutilisateur;
	protected $idclasse;
	protected $idspecialite;
	protected $ine;
	protected $dateinscription;
	protected $decision;
	protected $idadmin;

	public function __construct($id = null)
	{
		$this->db = new Database(_DB_HOST_, _DB_USER_, _DB_PASSWORD_);

		$this->definition = array(
			'table'   => array('Eleve e', 'Utilisateur u'),
			'primary' => 'idEleve',
			'join'    => array('e.idUtilisateur', 'u.idUtilisateur'),
			'fields'  => array('prenom', 'nom', 'tel', 'email', 'adresse', 'cp', 'ville', 'e.idUtilisateur', 'idClasse', 'idSpecialite', 'ine', 'dateInscription', 'decision', 'idAdmin')
		);

		if ($id) {
			$this->id = $id;

			$this->hydrate();
		}
	}

	public function getElevesAdmis($idClasse, $year)
	{
		$this->db->connect();

		$queryAdmis = "SELECT Admission.ElevesAdmis($idClasse, $year) AS admisCursor
			FROM dual";

		$resultsAdmis = $this->db->query($queryAdmis);

		$admis = array();
		while ($sysRefCursor = oci_fetch_assoc($resultsAdmis)) {
			// Execute return SYS_REFCURSOR
			$admisCursor = $sysRefCursor['ADMISCURSOR'];
			oci_execute($admisCursor);
		    while ($row = oci_fetch_assoc($admisCursor)) {  
		        $admis[] = array(
					'id'      => $row['IDUTILISATEUR'],
					'nom'     => $row['NOM'],
					'prenom'  => $row['PRENOM'],
					'moyenne' => $row['MOYENNE']
		        );
		    }
		    oci_free_statement($admisCursor);
		}

		return $admis;
	}

	public function getElevesRecales($idClasse, $year)
	{
		$this->db->connect();

		$queryRecales = "SELECT Admission.ElevesRecales($idClasse, $year) AS recalesCursor
			FROM dual";

		$resultsRecales = $this->db->query($queryRecales);

		$recales = array();
		while ($sysRefCursor = oci_fetch_assoc($resultsRecales)) {
			// Execute return SYS_REFCURSOR
			$recalesCursor = $sysRefCursor['RECALESCURSOR'];
			oci_execute($recalesCursor);
		    while ($row = oci_fetch_assoc($recalesCursor)) {  
		        $recales[] = array(
					'id'      => $row['IDUTILISATEUR'],
					'nom'     => $row['NOM'],
					'prenom'  => $row['PRENOM'],
					'moyenne' => $row['MOYENNE']
		        );
		    }
		    oci_free_statement($recalesCursor);
		}

		return $recales;
	}

	public function getMoyennes($semestre)
	{
		$this->db->connect();

		$queryMoyennes = "SELECT Notation.afficherMoyennesMatieres($this->idutilisateur, $semestre) AS moyennesCursor
			FROM dual";

		$resultsMoyennes = $this->db->query($queryMoyennes);

		$moyennes = array();
		while ($sysRefCursor = oci_fetch_assoc($resultsMoyennes)) {
			// Execute return SYS_REFCURSOR
			$moyennesCursor = $sysRefCursor['MOYENNESCURSOR'];
			oci_execute($moyennesCursor);
		    while ($row = oci_fetch_assoc($moyennesCursor)) {  
		        $moyennes[] = array(
					'libelle' => $row['LIBELLE'],
					'moyenne' => $row['MOYENNE'],
		        );
		    }
		    oci_free_statement($moyennesCursor);
		}

		return $moyennes;
	}
}