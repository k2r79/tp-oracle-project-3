<?php

require_once(_ROOT_PATH_.'core/Model.php');

/**
 * Utilisateur table model
 */
class Utilisateur extends Model
{
	protected $prenom;
	protected $nom;
	protected $tel;
	protected $email;
	protected $adresse;
	protected $cp;
	protected $ville;

	public function __construct($id = null)
	{
		$this->definition = array(
			'table'   => array('Utilisateur'),
			'primary' => 'idUtilisateur',
			'fields'  => array('prenom', 'nom', 'tel', 'email', 'adresse', 'cp', 'ville')
		);

		parent::__construct($id);
	}

	public function getPrenom()
	{
		return $this->prenom;
	}

	public function getNom()
	{
		return $this->nom;
	}
}