<?php

require_once(_ROOT_PATH_.'core/Model.php');

/**
 * Configuration table model
 */
class Semestre extends Model
{
	protected $libelle;
	protected $numsemestre;
	protected $datedebut;
	protected $datefin;

	public function __construct($id = null)
	{
		$this->definition = array(
			'table'   => array('Semestre'),
			'primary' => 'idSemestre',
			'fields'  => array('libelle', 'numSemestre', 'dateDebut', 'dateFin')
		);
		parent::__construct($id);
	}

	public function getLibelle()
	{
		return $this->libelle;
	}
}