<?php

define('_ROOT_PATH_', dirname(__FILE__).'/');

require_once(_ROOT_PATH_.'config/config.php');
require_once(_ROOT_PATH_.'core/Database.php');
require_once(_ROOT_PATH_.'core/Dispatcher.php');

$urlParts = array(
	'controller' => isset($_GET['controller']) ? $_GET['controller'] : null
);

$dispatcher = new Dispatcher();