<?php

// Debug Mode
define('_DEBUG_MODE_', true);

// Database constants
define('_DB_HOST_', '192.168.56.10:1521/xe');
define('_DB_USER_', 'EPSIBDD');
define('_DB_PASSWORD_', 'EPSIBDD');

// Application settings
define('_BASE_URL_', '/epsi/oracle-projet-3/');
define('_DEFAULT_CONTROLLER_', 'Home');
define('_DEFAULT_ACTION_', 'default');