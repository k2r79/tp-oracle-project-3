<?php

require_once(_ROOT_PATH_.'models/SemestreModel.php');

/**
 * Marks Display controller
 */
class NotesController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function moyennesAction($params = null)
	{
		$this->db->connect();

		if (isset($_GET['id'])) {
			$idEleve = $_GET['id'];
		}

		if (isset($_GET['id']) && isset($_GET['idsemestre'])) {
			$idSemestre = $_GET['idsemestre'];

			$eleve = new Eleve($idEleve);
			$data = array(
				'moyennes'   => $eleve->getMoyennes($idSemestre),
				'idSemestre' => $idSemestre,
				'showList'   => true
			);
		} else {
			$data = array(
				'showList' => false
			);
		}
		
		$data['idEleve'] = $idEleve;

		$semestre = new Semestre();
		$data['semestres'] = $semestre->getAll();

		die($this->render('moyennes', $data));
	}
}