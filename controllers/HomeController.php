<?php

/**
 * Homepage controller
 */
class HomeController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function defaultAction($params = null)
	{
		echo $this->render('default');
	}
}