<?php

/**
 * Admissions controller
 */
class AdmissionsController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function defaultAction($params = null)
	{
		$this->db->connect();

		if (isset($_GET['idclasse']) && isset($_GET['year'])) {
			$idClasse = $_GET['idclasse'];
			$year     = $_GET['year'];

			$eleve = new Eleve();
			$data = array(
				'admis'    => $eleve->getElevesAdmis($idClasse, $year),
				'recales'  => $eleve->getElevesRecales($idClasse, $year),
				'idClasse' => $idClasse,
				'year'     => $year,
				'showList' => true
			);
		} else {
			$data = array(
				'showList' => false
			);
		}

		die($this->render('default', $data));
	}
}