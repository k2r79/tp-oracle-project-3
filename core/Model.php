<?php

/**
 * Model definition class
 */
class Model
{
	protected $db;
	protected $definition;
	protected $id;

	public function __construct($id = null)
	{
		$this->db = new Database(_DB_HOST_, _DB_USER_, _DB_PASSWORD_);
		
		if ($id) {
			$this->id = $id;
			$this->hydrate();
		}
	}

	/**
	 * Hydrate the instance with the database values
	 */
	public function hydrate()
	{
		$this->db->connect();

		$query = 'SELECT '.implode(', ', $this->definition['fields']).' 
			FROM '.implode(', ', $this->definition['table']).'
			WHERE '.$this->definition['primary'].' = '.$this->id;
		if(isset($this->definition['join'])) {
			$query .= ' AND '.implode(' = ', $this->definition['join']);
		}

		$results = $this->db->getRow($query);

		foreach ($results as $key => $result) {
			$attribute = explode('.', strtolower($key));
			$attribute = end($attribute);
			$this->$attribute = $result;
		}

		$this->db->close();
	}

	/**
	 * Return all the model objects in the database
	 * @return array Array of models
	 */
	public function getAll()
	{
		$this->db = new Database(_DB_HOST_, _DB_USER_, _DB_PASSWORD_);
		$this->db->connect();

		$query = 'SELECT '.$this->definition['primary'].', '.implode(', ', $this->definition['fields']).' 
			FROM '.implode(', ', $this->definition['table']);
		if(isset($this->definition['join'])) {
			$query .= ' WHERE '.implode(' = ', $this->definition['join']);
		}

		$results = $this->db->query($query);

		$class = get_class($this);
		$return = array();
		while ($row = oci_fetch_assoc($results)) {
			$class = new $class($row[strtoupper($this->definition['primary'])]);
			$return[] = $class;
		}

		$this->db->close();

		return $return;
	}

	public function getId()
	{
		return $this->id;
	}
}