<?php

/**
 * Database Managment Class
 */
class Database
{
	private $host;
	private $user;
	private $password;
	private $connection;

	public function __construct($host, $user, $password)
	{
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
	}

	public function connect()
	{
		$this->connection = oci_connect($this->user, $this->password, $this->host);
		
		if (!$this->connection) {
			if (_DEBUG_MODE_) {
				$m = oci_error();
		   		echo $m['message'], "\n";
		   		return false;
			}
		}

		return true;
	}

	public function close()
	{
		oci_close($this->connection);
	}

	/**
	 * Executes the specified query
	 * @param string $query The query
	 * @return array Query results
	 */
	public function query($query)
	{
		if ($query) {
			$queryParser = oci_parse($this->connection, $query);
			oci_execute($queryParser);

			return $queryParser;
		} else {
			if (_DEBUG_MODE_) {
				die('Your query is empty or invalid');
			}
		}
	}

	/**
	 * Executes the specified query and returns one row
	 * @param string $query The query
	 * @return array Query results
	 */
	public function getRow($query)
	{
		if ($query) {
			$queryParser = oci_parse($this->connection, $query);
			oci_execute($queryParser);

			return oci_fetch_assoc($queryParser);
		} else {
			if (_DEBUG_MODE_) {
				die('Your query is empty or invalid');
			}
		}
	}
}