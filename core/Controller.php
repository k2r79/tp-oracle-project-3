<?php

require_once(_ROOT_PATH_.'lib/Twig/Autoloader.php');
require_once(_ROOT_PATH_.'models/ConfigurationModel.php');
require_once(_ROOT_PATH_.'models/ClasseModel.php');

/**
 * Controller structure definition class
 */
class Controller
{
	protected $twig;
	protected $templateData;
	protected $db;
	private $controllerName;

	public function __construct()
	{
		$this->init();
	}

	/**
	 * Initializes the necessary components for the controller
	 */
	public function init()
	{
		// Define the base url
		$this->templateData = array('_base_url_' => _BASE_URL_);

		// Initialize the database connection
		$this->db = new Database(_DB_HOST_, _DB_USER_, _DB_PASSWORD_);
		$this->db->connect();

		// Get school's name
		$configuration = new Configuration();
		$this->templateData['_school_name_'] = $configuration->get('_SCHOOL_NAME_');

		// Get the classes
		$classes = new Classe();
		$classes = $classes->getAll();
		$this->templateData['_school_classes_'] = $classes;

		// Only get the controller's name
		$this->controllerName = strtolower(str_replace('Controller', '', get_class($this)));

		// Check if the view folder exists
		if (file_exists(_ROOT_PATH_.'views')) {
			// Load Twig
			Twig_AutoLoader::register();
			$twigLoader = new Twig_Loader_Filesystem(_ROOT_PATH_.'views');
			$this->twig = new Twig_Environment($twigLoader, array(
			    'cache' => _DEBUG_MODE_ ? false : _ROOT_PATH_.'cache/twig',
			    'debug' => _DEBUG_MODE_ ? true : false,
			));
		} else {
			if (_DEBUG_MODE_) {
				die('The template directory '._ROOT_PATH_.'views/ does not exist');
			} else {
				Dispatcher::generate404();
			}
		}
	}

	/**
	 * Default controller action
	 * @param array $params URL parameters
	 */
	public function defaultAction($params = null)
	{
		$this->render();
	}

	/**
	 * Render the controller's template
	 * @param string $tplPath Custom template's path
	 */
	public function render($tpl = 'default', $data = null)
	{
		$data = $data ? array_merge($this->templateData, $data) : $this->templateData;

		if (file_exists(_ROOT_PATH_.'views/'.$this->controllerName.'/'.$tpl.'.html.twig')) {
			echo $this->twig->render($this->controllerName.'/'.$tpl.'.html.twig', $data);
		} else {
			if (_DEBUG_MODE_) {
				die('The template file '._ROOT_PATH_.'views/'.$this->controllerName.'/'.$tpl.'.html.twig does not exist');
			} else {
				Dispatcher::generate404();
			}
		}

		$this->db->close();
	}
}