<?php

require_once(_ROOT_PATH_.'core/Controller.php');

/**
 * Dispatcher class which handles routes
 */
class Dispatcher
{
	private $controller;
	private $action;
	private $params;

	public function __construct()
	{
		// Analyze URL
		$this->controller = $this->getControllerFromUrl();
		$this->getActionFromUrl();

		// Call controller's method
		$action = $this->action;
		$this->controller->$action($_GET);
	}

	/**
	 * Collects the controller from the URL
	 */
	private function getControllerFromUrl()
	{
		// Makes sure the controller's name is in the specified
		$controller = isset($_GET['controller']) ? stripslashes($_GET['controller']) : null; 
		
		if (!$controller) {
			$controller = _DEFAULT_CONTROLLER_.'Controller';

			require_once(_ROOT_PATH_.'controllers/'.$controller.'.php');
		} else {
			// Makes sure the controller's name is in the right case
			$controller = ucfirst(strtolower($controller)).'Controller';

			// Check if the controller exists
			if (file_exists(_ROOT_PATH_.'controllers/'.$controller.'.php')) {
				require_once(_ROOT_PATH_.'controllers/'.$controller.'.php');
			} else {
				if (_DEBUG_MODE_) {
					die('The controller '.$controller.' doesn\'t exist');
				} else {
					self::generate404();
				}
			}
		}

		return new $controller();
	}

	/**
	 * Collects the action of the controller from the URL
	 * @return string The controller's action name
	 */
	private function getActionFromUrl()
	{
		// Makes sure the action's name is specified
		if (isset($_GET['action'])) {
			$this->action = $_GET['action'] ? stripslashes($_GET['action']).'Action' : null; 
		} 

		if (!$this->action) {
			$this->action = _DEFAULT_ACTION_.'Action';
		}

		// Check if the action exists
		if (!method_exists($this->controller, $this->action)) {
			if (_DEBUG_MODE_) {
				die('The action '.$this->action.' doesn\'t exist in controller '.get_class($this->controller));
			} else {
				self::generate404();
			}
		}
	}

	/**
	 * Generate a 404 error page
	 */
	public static function generate404()
	{
		return header("HTTP/1.0 404 Not Found");
	}
}